// ---------------
// test_Grades.cpp
// ---------------

// --------
// includes
// --------

#include <map>    // map
#include <string> // string
#include <vector> // vector

#include "gtest/gtest.h"

#include "Grades.hpp"
#include "run_Grades.cpp"

using namespace std;

// ----------------
// test_grades_eval
// ----------------

TEST(grades_eval, test_0) {
    const vector<vector<int>> v_v_scores = {{0, 0, 0, 0, 0}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "F");
}

TEST(grades_eval, test_1) {
    const vector<vector<int>> v_v_scores = {{5, 12, 14, 14, 42}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "A");
}
TEST(grades_eval, test_2) {
    const vector<vector<int>> v_v_scores = {{5, 11, 13, 13, 38}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "A-");
}

TEST(grades_eval, test_3) {
    const vector<vector<int>> v_v_scores = {{4, 10, 12, 12, 37}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B+");
}

TEST(grades_eval, test_4) {
    const vector<vector<int>> v_v_scores = {{4, 10, 12, 12, 35}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B");
}

TEST(grades_eval, test_5) {
    const vector<vector<int>> v_v_scores = {{4, 10, 11, 11, 34}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B-");
}

TEST(grades_eval, test_6) {
    const vector<vector<int>> v_v_scores = {{4, 9, 11, 11, 32}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "C+");
}

TEST(grades_eval, test_7) {
    const vector<vector<int>> v_v_scores = {{4, 9, 10, 10, 31}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "C");
}

TEST(grades_eval, test_8) {
    const vector<vector<int>> v_v_scores = {{4, 8, 10, 10, 29}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "C-");
}

TEST(grades_eval, test_9) {
    const vector<vector<int>> v_v_scores = {{3, 8, 9, 9, 28}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "D+");
}

TEST(grades_eval, test_10) {
    const vector<vector<int>> v_v_scores = {{3, 8, 9, 9, 27}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "D");
}

TEST(grades_eval, test_11) {
    const vector<vector<int>> v_v_scores = {{3, 7, 8, 8, 25}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "D-");
}

TEST(grades_eval, test_12) {
    const vector<vector<int>> v_v_scores = {{2, 12, 14, 14, 42}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "F");
}

// ----------------
// test_grades_read
// ----------------

TEST(grades_read, test_13) {
    std::istringstream input("\n");
    std::cin.rdbuf(input.rdbuf());
    std::vector<std::vector<int>> expected(5, std::vector<int>());
    std::vector<std::vector<int>> result = grades_read();
    ASSERT_EQ(result, expected);
}

TEST(grades_read, test_14) {
    std::istringstream input("1 2 3 4 5\n6 7 8 9 10\n11 12 13 14 15\n16 17 18 19 20\n21 22 23 24 25\n");
    std::cin.rdbuf(input.rdbuf());
    std::vector<std::vector<int>> expected = {
        {1, 2, 3, 4, 5},
        {6, 7, 8, 9, 10},
        {11, 12, 13, 14, 15},
        {16, 17, 18, 19, 20},
        {21, 22, 23, 24, 25}
    };
    std::vector<std::vector<int>> result = grades_read();
    ASSERT_EQ(result, expected);
}

TEST(grades_read, test_15) {
    std::istringstream input("a b c d e\nf g h i j\nk l m n o\np q r s t\nu v w x y\n");
    std::cin.rdbuf(input.rdbuf());

    std::vector<std::vector<int>> expected(5, std::vector<int>());
    std::vector<std::vector<int>> result = grades_read();

    ASSERT_EQ(result, expected);
}

TEST(grades_read, test_16) {
    std::istringstream input("2 1 1 2 2\n"
                             "1 2 3 2 0 0 2 3 3 0 1 2\n"
                             "0 3 1 2 0 3 2 1 0 1 1 1 3 3\n"
                             "1 0 3 1 1 2 0 0 0 2 1 3 1 3\n"
                             "3 1 0 2 2 2 0 0 1 0 0 2 1 1 2 1 2 2 3 1 3 1 0 3 1 0 3 3 0 3 2 1 1 0 3 1 3 0 3 3 3 3");

    std::cin.rdbuf(input.rdbuf());
    std::vector<std::vector<int>> expected = {
        {2, 1, 1, 2, 2},
        {1, 2, 3, 2, 0, 0, 2, 3, 3, 0, 1, 2},
        {0, 3, 1, 2, 0, 3, 2, 1, 0, 1, 1, 1, 3, 3},
        {1, 0, 3, 1, 1, 2, 0, 0, 0, 2, 1, 3, 1, 3},
        {3, 1, 0, 2, 2, 2, 0, 0, 1, 0, 0, 2, 1, 1, 2, 1, 2, 2, 3, 1, 3, 1, 0, 3, 1, 0, 3, 3, 0, 3, 2, 1, 1, 0, 3, 1, 3, 0, 3, 3, 3, 3}
    };
    std::vector<std::vector<int>> result = grades_read();
    ASSERT_EQ(result, expected);
}

// ----------------
// test_grades_print
// ----------------

TEST(grades_print, test_17) {
    testing::internal::CaptureStdout();
    grades_print("A");
    std::string output = testing::internal::GetCapturedStdout();
    ASSERT_EQ(output, "A\n");
}

TEST(grades_print, test_18) {
    testing::internal::CaptureStdout();
    grades_print("B+");
    std::string output = testing::internal::GetCapturedStdout();
    ASSERT_EQ(output, "B+\n");
}

TEST(grades_print, test_19) {
    testing::internal::CaptureStdout();
    grades_print("C-");
    std::string output = testing::internal::GetCapturedStdout();
    ASSERT_EQ(output, "C-\n");
}

TEST(grades_print, test_20) {
    testing::internal::CaptureStdout();
    grades_print("F");
    std::string output = testing::internal::GetCapturedStdout();
    ASSERT_EQ(output, "F\n");
}
TEST(grades_print, test_21) {
    testing::internal::CaptureStdout();
    grades_print("");
    std::string output = testing::internal::GetCapturedStdout();
    ASSERT_EQ(output, "\n");
}