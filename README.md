# CS371p: Object-Oriented Programming Grades Repo

* Name: Kevin Pham

* EID: kmp4393

* GitLab ID: Rhirekith

* HackerRank ID: kevpham_cs

* Git SHA: da3cfabaf902bacf765e684d6fab2052344ef242

* GitLab Pipelines: https://gitlab.com/Rhirekith/cs371p-grades/-/pipelines/1150513604

* Estimated completion time: 4

* Actual completion time: 3

* Comments: N/A
