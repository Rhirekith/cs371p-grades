// ----------
// Grades.hpp
// ----------

#ifndef Grades_hpp
#define Grades_hpp

// --------
// includes
// --------

#include <algorithm> // count, min_element, transform
#include <cassert>   // assert
#include <map>       // map
#include <string>    // string
#include <vector>    // vector

using namespace std;

// -----------
// grades_eval
// -----------

string grades_eval (const vector<vector<int>>& v_v_scores) {
    // assert(&v_v_scores);
    vector<int> scores(5);
    // Declarations of local variables
    int count = 0;
    int total = 0;
    int amt_three = 0;
    int amt_one = 0;
    // Double for each loop
    for (const auto& category_scores : v_v_scores) {
        for (int score : category_scores) {
            // Checking if score meets the expectation in syllabus
            if (score >= 2)
                total++;
            // Count # of 3s
            if (score == 3)
                amt_three++;
            // Count # of 1s
            if (score == 1) 
                amt_one++;
        }
        // 3s can make up 1s for exercises, papers, quizzes
        // Add the min of amt_three / 2 and amt_one to the category scores
        total += min (amt_three / 2, amt_one);
        // cout << total << " "; 
        // Add the scores to a vector
        scores[count] = total;
        // Increment count
        count++;
        // Reinitialize variables to zero
        total = 0;
        amt_three = 0;
        amt_one = 0;
    }
    // Find out which letter grade student received
    if (scores[0] == 5 && scores[1] >= 11 && scores[2] >= 13 && scores[3] >= 13 && scores[4] >= 39) {
        return "A";
    } else if (scores[0] == 5 && scores[1] >= 11 && scores[2] >= 13 && scores[3] >= 13 && scores[4] >= 38) {
        return "A-";
    } else if (scores[0] >= 4 && scores[1] >= 10 && scores[2] >= 12 && scores[3] >= 12 && scores[4] >= 37) {
        return "B+";
    } else if (scores[0] >= 4 && scores[1] >= 10 && scores[2] >= 12 && scores[3] >= 12 && scores[4] >= 35) {
        return "B";
    } else if (scores[0] >= 4 && scores[1] >= 10 && scores[2] >= 11 && scores[3] >= 11 && scores[4] >= 34) {
        return "B-";
    } else if (scores[0] >= 4 && scores[1] >= 9 && scores[2] >= 11 && scores[3] >= 11 && scores[4] >= 32) {
        return "C+";
    } else if (scores[0] >= 4 && scores[1] >= 9 && scores[2] >= 10 && scores[3] >= 10 && scores[4] >= 31) {
        return "C";
    } else if (scores[0] >= 4 && scores[1] >= 8 && scores[2] >= 10 && scores[3] >= 10 && scores[4] >= 29) {
        return "C-";
    } else if (scores[0] >= 3 && scores[1] >= 8 && scores[2] >= 9 && scores[3] >= 9 && scores[4] >= 28) {
        return "D+";
    } else if (scores[0] >= 3 && scores[1] >= 8 && scores[2] >= 9 && scores[3] >= 9 && scores[4] >= 27) {
        return "D";
    } else if (scores[0] >= 3 && scores[1] >= 7 && scores[2] >= 8 && scores[3] >= 8 && scores[4] >= 25) {
        return "D-";
    } else {
        return "F";
    }
}

#endif // Grades_hpp
