.DEFAULT_GOAL := all
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := clang++
    CXXFLAGS      := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := llvm-cov gcov
    GTEST         := /usr/local/include/gtest
    LDFLAGS       := -lgtest -lgtest_main
    LIB           := $(LIBRARY_PATH)
    VALGRIND      :=
else ifeq ($(shell uname -p), x86_64)
    ASTYLE        := astyle
    BOOST         := /lusr/opt/boost-1.82/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-11
    CXXFLAGS      := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := gcov-11
    GTEST         := /usr/include/gtest
    LDFLAGS       := -L/usr/local/opt/boost-1.77/lib/ -lgtest -lgtest_main -pthread
    LIB           := /usr/lib/x86_64-linux-gnu
    VALGRIND      := valgrind-3.17
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := gcov
    GTEST         := /usr/include/gtest
    LDFLAGS       := -lgtest -lgtest_main -pthread
    LIB           := /usr/lib
    VALGRIND      := valgrind
endif

# run docker
docker:
	docker run --rm -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Grades.log.txt:
	git log > Grades.log.txt

# get git status
status:
	make --no-print-directory clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Grades code repo
pull:
	make --no-print-directory clean
	@echo
	git pull
	git status

# upload files to the Grades code repo
push:
	make --no-print-directory clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Grades.ctd.txt
	git add Grades.hpp
	-git add Grades.log.txt
	-git add html
	git add Makefile
	git add README.md
	git add run_Grades.cpp
	git add test_Grades.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
run_Grades: Grades.hpp run_Grades.cpp
	-$(CPPCHECK) run_Grades.cpp
	$(CXX) $(CXXFLAGS) run_Grades.cpp -o run_Grades

# compile test harness
test_Grades: Grades.hpp test_Grades.cpp
	-$(CPPCHECK) test_Grades.cpp
	$(CXX) $(CXXFLAGS) test_Grades.cpp -o test_Grades $(LDFLAGS)

# run/test files, compile with make all
FILES :=        \
    run_Grades  \
    test_Grades

# compile all
all: $(FILES)

# execute test harness with coverage
test: test_Grades
	$(VALGRIND) ./test_Grades
ifeq ($(shell uname -s), Darwin)
	$(GCOV) test_Grades-test_Grades.cpp | grep -B 2 "cpp.gcov"
else
	$(GCOV) test_Grades.cpp | grep -B 2 "cpp.gcov"
endif

# clone the Grades test repo
../cs371p-grades-tests:
	git clone https://gitlab.com/gpdowning/cs371p-grades-tests.git ../cs371p-grades-tests

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g Grades.ctd.txt >> Grades.gen.txt

# execute the run harness against your test files in the Grades test repo and diff with the expected output
# change gpdowning to your GitLab-ID
run: run_Grades ../cs371p-grades-tests
	$(CHECKTESTDATA) Grades.ctd.txt ../cs371p-grades-tests/gpdowning-Grades.in.txt
	./run_Grades < ../cs371p-grades-tests/gpdowning-Grades.in.txt > Grades.tmp.txt
	diff Grades.tmp.txt ../cs371p-grades-tests/gpdowning-Grades.out.txt

# execute the run harness against all of the test files in the Grades test repo and diff with the expected output
run-all: run_Grades ../cs371p-grades-tests
	-@for v in `ls ../cs371p-grades-tests/*.in.txt`;         \
    do                                                              \
        echo $(CHECKTESTDATA) Grades.ctd.txt $${v};          \
             $(CHECKTESTDATA) Grades.ctd.txt $${v};          \
        echo ./run_Grades \< $${v} \> Grades.tmp.txt; \
             ./run_Grades  < $${v}  > Grades.tmp.txt; \
        echo diff Grades.tmp.txt $${v/.in/.out};             \
             diff Grades.tmp.txt $${v/.in/.out};             \
    done

# auto format the code
format:
	$(ASTYLE) Grades.hpp
	$(ASTYLE) run_Grades.cpp
	$(ASTYLE) test_Grades.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check the existence of check files
check: .gitignore .gitlab-ci.yml Grades.log.txt html

# remove executables and temporary files
clean:
	rm -f  *.gcda
	rm -f  *.gcno
	rm -f  *.gcov
	rm -f  *.gen.txt
	rm -f  *.tmp.txt
	rm -f  $(FILES)
	rm -rf *.dSYM

# remove executables, temporary files, and generated files
scrub:
	make --no-print-directory clean
	rm -f  Grades.log.txt
	rm -f  Doxyfile
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	uname -p

	@echo
	uname -s

	@echo
	which $(ASTYLE)
	@echo
	$(ASTYLE) --version

	@echo
	which $(CHECKTESTDATA)
	@echo
	$(CHECKTESTDATA) --version | head -n 1

	@echo
	which cmake
	@echo
	cmake --version | head -n 1

	@echo
	which $(CPPCHECK)
	@echo
	$(CPPCHECK) --version

	@echo
	which $(DOXYGEN)
	@echo
	$(DOXYGEN) --version

	@echo
	which $(CXX)
	@echo
	$(CXX) --version | head -n 1

	@echo
	which $(GCOV)
	@echo
	$(GCOV) --version | head -n 1

	@echo
	which git
	@echo
	git --version

	@echo
	which make
	@echo
	make --version | head -n 1

ifneq ($(VALGRIND),)
	@echo
	which $(VALGRIND)
	@echo
	$(VALGRIND) --version
endif

	@echo
	which vim
	@echo
	vim --version | head -n 1

	@echo
	grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	ls -al $(GTEST)/gtest.h
	@echo
	pkg-config --modversion gtest
	@echo
	ls -al $(LIB)/libgtest*.a
